class CesarCrypting:
    def __init__(self, shift_num):

        if type(shift_num) == int:
            self.shift_num = shift_num
        else:
            raise ValueError
    
    def encrypting(self, text):
        crypted_text = ""
        for char in text:
            if char.isalpha():
                base = ord('A') if char.isupper() else ord('a')
                unicode_index = (ord(char) - base + self.shift_num) % 26 + base
                crypted_text += chr(unicode_index)
            else:
                crypted_text += char
        return crypted_text
    
    def decrypting(self, crypted_text):
        decrypted_text = ""
        for char in crypted_text:
            if char.isalpha():
                base = ord('A') if char.isupper() else ord('a')
                unicode_index = (ord(char) - base - self.shift_num) % 26 + base
                decrypted_text += chr(unicode_index)
            else:
                decrypted_text += char
        return decrypted_text

if __name__=='__main__':

    cesar = CesarCrypting(13)

    crypted = cesar.encrypting('matheus')
    print(f'Mensagem criptografada: {crypted}')
    decrypted = cesar.decrypting(crypted)
    print(f'Mensagem descriptografada: {decrypted}')
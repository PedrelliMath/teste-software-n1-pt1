import pytest
from main import CesarCrypting

def test_instance_success():
    cesar = CesarCrypting(13)
    assert isinstance(cesar, CesarCrypting)

def test_instance_failure():
    with pytest.raises(ValueError):
        cesar = CesarCrypting('m')

def test_instance_shifting_value():
    cesar = CesarCrypting(13)
    assert cesar.shift_num == 13

def test_hasEncrypting():
    cesar = CesarCrypting(13)
    assert hasattr(cesar, 'encrypting')

def test_hasDecrypting():
    cesar = CesarCrypting(13)
    assert hasattr(cesar, 'decrypting')
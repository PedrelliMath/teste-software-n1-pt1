import pytest 
from main import CesarCrypting

def test_decrypting_shifting_13():
    cesar = CesarCrypting(13)
    assert 'matheus' == cesar.decrypting('zngurhf')

def test_decrypting_shifting_100():
    cesar = CesarCrypting(100)
    assert 'matheus' == cesar.decrypting('iwpdaqo')

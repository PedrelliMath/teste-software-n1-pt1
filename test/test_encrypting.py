import pytest 
from main import CesarCrypting

def test_encrypting_shifting_13():
    cesar = CesarCrypting(13)
    assert 'zngurhf' == cesar.encrypting('matheus')

def test_encrypting_shifting_100():
    cesar = CesarCrypting(100)
    assert 'iwpdaqo' == cesar.encrypting('matheus')

def test_encrypting_shifting_13_numeric():
    cesar = CesarCrypting(13)
    assert '1' == cesar.encrypting('1')

def test_encrypting_shifting_13_numeric_2():
    cesar = CesarCrypting(13)
    assert '1111111111' != cesar.encrypting('matheus')
